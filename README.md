# README #

人力飛行機制作テスト用プログラム

### 各コード説明 ###

**Acceleration**

-accel.ino

加速度センサのテスト運転プログラム


**EEPROM**

-read.ino

一時的なデータを読み込むテストプログラム

-write.ino

一時的にデータを書き込むプログラム


**Encoder**

-encoder.ino

エンコーダの値を読み込むテスト運転プログラム


**Hall_IC**

-Hall_IC.ino

1分間の回転数を測るテスト運転プログラム(ホールICのテスト運転)


**LED**

-led_ber_meter.ino

LEDを10個並べて一つずつ点灯させるプログラム

-serial_pararel.ino

シリアルパラレル変換プログラム


**SDcard**

　-SD_write.ino

　　SDカードに入っているファイルを読み込み、書き込むプログラム


**Servo**

　-servo.ino

　　サーボモータの傾きを表示するプログラム

　-servo_move.ino

　　サーボモータを左右に動かすプログラム


**Ultra_Sonic**

　-Ultra_Sonic.ino

　　超音波センサのテスト運用プログラム


**main_code**

　Sencer - Sencer.ino

　　回転数計と高度計の値をとり、SDカードに保存するプログラム

　Steering - Steering.ino

　　4つのサーボモータを動かすプログラム