#include <Servo.h>

//analog
int aile_R_joy = A2;
int aile_L_joy = A3;
int elev_joy = A4;
int lad_joy = A5;

//digital
Servo aile_R;
Servo aile_L;
Servo elev;
Servo lad;

int R_val = 0;
int L_val = 0;
int E_val = 0;
int LA_val = 0;

//testしたらこの部分は削除
int test;
//

void setup(){
  aile_R.attach(10);
  aile_L.attach(13);
  elev.attach(2);
  lad.attach(4);
  
 // Serial.begin(9600);
}

void loop(){
  //Serial.println("//////////////////");
  
  
  //Serial.print("R: ");
  Joy_Servo_InOut(R_val, aile_R_joy, aile_R);
  
  
  //Serial.print("L: ");
  Joy_Servo_InOut(L_val, aile_L_joy, aile_L);

  
  //Serial.print("E: ");
  Joy_Servo_InOut(E_val, elev_joy, elev);

  
 // Serial.print("LA: ");
  Joy_Servo_InOut(LA_val, lad_joy, lad);
  //Serial.println("");

}

void Joy_Servo_InOut(int J_val, int joyPin, Servo servo){
    J_val = analogRead(joyPin);
    J_val = map(J_val, 0, 1023, 1500, 1900);
    // J_val = map(J_val, 0, 1023, 0, 180);
    servo.writeMicroseconds(J_val);
    //Serial.println(J_val);
  
    
    //test
   /*
    test = servo.read();
    Serial.println(test);
    */
    
}
